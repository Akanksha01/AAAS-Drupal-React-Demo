# Drupal-React Implementation

This repo represents the integration of Drupal web services in a React application.

## Prerequites/Dependencies

- Node 8 (dev) - [Can be managed via nvm](https://github.com/creationix/nvm)
- [webpack](https://github.com/webpack) installed globally.
- [webpack-cli](https://github.com/webpack/webpack-cli) installed globally.

## Install

- If you don't have webpack installed globally yet, `npm install webpack -g`
- If you don't have webpack-cli installed globally yet, `npm install webpack-cli -g`
- Install libraries `npm install`

## Application Description

- The application uses ReactJs and Redux.
- The repository consists of the article listing and details components as depicted in `http://sciencemag.org/news`. 
- There is a usage of common header and footer which will be displayed on all the pages. 
- Application uses React Routing for Traversing from one page to another.

## SEO implemetation

- Usage of React Helmet for the itegration of meta-tags for SEO optimization.

## Styles

The styles for this project are maintained in (`src/assets/stylesheets/style.css`).