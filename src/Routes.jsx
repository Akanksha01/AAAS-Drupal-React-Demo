import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Articles from './containers/Articles';
import ArticleDetails from './containers/ArticleDetails';

const Routes = () => (
    <BrowserRouter>
      <Switch>
        <Route exact path='/news' component={Articles}/>
        <Route path="/news/:year/:month/:id/:title" component={ArticleDetails}/>
      </Switch>
    </BrowserRouter>
)

export default Routes;
