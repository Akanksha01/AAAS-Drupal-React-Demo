import React, { Component } from 'react';

class Share extends Component {

  render(){
    return(
      <div className="tertiary">
        <div id="undefined-sticky-wrapper" className="sticky-wrapper is-sticky" >
          <div className="article__social fixedsticky" >
            <h4 className="share-title">Share</h4>
            <ul className="socialshares">
              <li>
                <a className="social__icon social__icon--facebook">
                  <i className="fa fa-facebook fa-lg"></i>
                  <span className="sr-only">Share on facebook</span>
                </a>
                <span className="social__count">220</span>
              </li>
              <li>
                <a className="social__icon social__icon--twitter">
                  <i className="fa fa-twitter fa-lg"></i>
                  <span className="sr-only">Share on twitter</span>
                </a>
                <span className="social__count social__count--empty">&nbsp;</span>
              </li>
              <li>
                <a className="social__icon social__icon--reddit">
                  <i className="fa fa-reddit fa-lg"></i>
                  <span className="sr-only">Share on reddit</span>
                </a>
                <span className="social__count">12</span>
              </li>
              <li>
                <a className="social__icon social__icon--linkedin">
                  <i className="fa fa-linkedin fa-lg"></i>
                  <span className="sr-only">Share on linkedin</span>
                </a>
                <span className="social__count social__count--empty">&nbsp;</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Share;

