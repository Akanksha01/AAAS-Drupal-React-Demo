import React, { Component } from 'react';
import NavigationList from './NavigationList' ;

class Navigation extends Component {

	render(){
		return(
			<nav className="sticky-header__nav">
      	<NavigationList/>
   		</nav>
		);
	}
}

export default Navigation;
