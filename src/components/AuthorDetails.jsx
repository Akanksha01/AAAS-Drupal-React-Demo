import React, { Component } from 'react';

class AuthorDetails extends Component {

  render(){
    
    return(
      <div className="author__card ">
        <div className="author__icon">
          <a><img src="//www.sciencemag.org/sites/default/files/styles/contributor_thumb_sq/public/contributor-images/Mervis.jpg?itok=txIdw4vg" alt="author"/></a>  
        </div>
        <div className="author__cardbody">
          <h3 className="author__name">
            <a>Jeffrey Mervis</a>
          </h3>
          <span property="dc:title" content="Jeffrey Mervis" className="rdf-meta element-hidden"></span>
          <p>Jeff tries to explain how government works to readers of <em>Science</em>.</p>
          <ul className="author__follow">
            <li>
              <a href="mailto:jmervis@aaas.org">
                <i className="fa fa-fw fa-lg fa-envelope"></i> Email Jeffrey
              </a>  
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default AuthorDetails;

