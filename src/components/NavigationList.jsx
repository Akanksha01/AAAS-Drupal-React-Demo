import React, { Component } from 'react';
import ListItems from './shared/ListItems' ;

class Navigation extends Component {

	render(){
		return(
      	<ul className="sticky-header__navlist">
          <ListItems className="sticky-header__navitem" redirectTo="/" title="Home"/>
          <ListItems className="sticky-header__navitem" redirectTo="/news" title="News"/>
        </ul>
		);
	}
}

export default Navigation;

