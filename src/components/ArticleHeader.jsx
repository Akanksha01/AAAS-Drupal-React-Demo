import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LinkItems from './shared/LinkItems';
import Time from './shared/Time';

class ArticleHeader extends Component {

  static propTypes = {
    title: PropTypes.string,
    articleDate: PropTypes.string,
    authername: PropTypes.string
  };

  render(){
    const {title, articleDate, authername}=this.props;
    
    return(
      <header className="article__header article__header--inline">
        <h1 className="article__headline">{title}</h1>
        <p className="byline byline--article">By{" "} 
          <LinkItems title={authername} redirectTo="# "/>
          <Time title={articleDate}/>
        </p>
      </header>
    );
  }
}

export default ArticleHeader;

