import React, { Component } from 'react';
import Input from './shared/Input';
import Label from './shared/Label';

class SearchBox extends Component {

	render(){
		return(
			<div className="sticky-header__controls">
				<form className="form-inline form-search--sticky">
					<div className="field-container--inline">
						<label className="element-invisible" htmlFor="edit-aaas-search-block-form--8">Search </label>
						<Input title="Enter the terms you wish to search for." className="search-field" placeholder="Search" type="text" id="edit-aaas-search-block-form--8" value="" size="15" maxlength="128" />
						<button className="btn search-submit" type="submit" id="edit-submit--4" name="">
							<span className="icon-search" aria-hidden="true"></span>
							<span className="is-vishidden">Search</span>
							<i className="fa fa-search"></i>
						</button>
					</div>
				</form>
				<Label className="search-toggler__trigger" htmlFor="search-toggler" fontClassName="fa fa-search"/>
				<a href="#page-top" className="btn--uptoggle">
					<i className="fa fa-long-arrow-up"></i>
				</a>
				<Label className="menu-toggler__trigger" htmlFor="menu-toggler" fontClassName="fa fa-bars"/>
			</div>
		);
	}
}

export default SearchBox;
