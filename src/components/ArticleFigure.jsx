import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ArticleFigure extends Component {

  static propTypes = {
    imageUrl: PropTypes.string,
    bannerTitle: PropTypes.string
  };

  render(){
    const {imageUrl, bannerTitle}=this.props;
    
    return(
      <figure className="figure">
        <div className="figure__head">
          <img src={imageUrl} alt="figure"/>
        </div>
        <figcaption>
          <div className="caption">
            <div className="caption__text">
              <p>{bannerTitle}</p>
            </div>
          </div>
        </figcaption>
      </figure>
    );
  }
}

export default ArticleFigure;

