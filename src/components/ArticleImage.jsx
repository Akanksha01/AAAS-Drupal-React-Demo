import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ArticleImage extends Component {

	static propTypes = {
    url: PropTypes.string,
    alt: PropTypes.string,
    detailURL: PropTypes.string
  };

	render(){
		const {url, alt, detailURL}=this.props;

		return(
        <div className="media__icon">
          <a href={detailURL}>
            <img src={url} alt={alt}></img>
          </a>  
        </div>
		);
	}
}

export default ArticleImage;
