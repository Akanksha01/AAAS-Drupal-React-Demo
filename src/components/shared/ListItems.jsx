import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LinkItems from './LinkItems';

class ListItems extends Component {

  static propTypes = {
    className: PropTypes.string,
    redirectTo: PropTypes.string,
    title: PropTypes.string
  };

	render(){
    const {className, redirectTo, title} = this.props;

		return(
      <li className={className}>
        <LinkItems redirectTo={redirectTo} title={title}/>
      </li>
		);
	}
}

export default ListItems;
