import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Time extends Component {

  static propTypes = {
    title: PropTypes.string
  };

	render(){
    const {title} = this.props;

		return(
      <time>{title}</time>
		);
	}
}

export default Time;
