import React, { Component } from 'react';

class AAASFooter extends Component {

  render(){
    
    return(
      <div className="row row--footer" lang="en">
        <div className="container container--footer">
          <div className="footer__header">
            <div className="site-logo--footer">
              <a href="https://www.aaas.org"><img alt="AAAS" title="American Association for the Advancement of Science" src="http://www.sciencemag.org/sites/all/themes/science/images/logo-aaas.svg"/></a>
            </div>
          </div>
          <div className="footer__body">
            <p className="copyright-statement">© 2018&nbsp;<a href="http://www.aaas.org/">American Association for the Advancement of Science</a>. All rights Reserved. AAAS is a partner of <a href="http://www.who.int/hinari/en/">HINARI</a>, <a href="http://www.fao.org/agora/en/">AGORA</a>, <a href="http://www.unep.org/oare/">OARE</a>,&nbsp;<a href="http://www.chorusaccess.org/">CHORUS</a>, <a href="http://www.clockss.org/">CLOCKSS</a>, <a href="http://www.crossref.org/">CrossRef</a> and <a href="http://www.projectcounter.org/">COUNTER</a>.</p>
            <ul className="footlinks">
              <li><a href="http://www.sciencemag.org/about/terms-service" title="">Terms of Service</a></li>
              <li><a href="http://www.sciencemag.org/about/privacy-policy" title="">Privacy Policy</a></li>
              <li><a href="http://www.sciencemag.org/about/contact-us" title="">Contact Us</a></li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default AAASFooter;

