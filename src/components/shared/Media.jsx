import React, { Component } from 'react';

class Media extends Component {

	render(){

		return(
				<div className="video-player box-standout layout-item">
				  <div className="video-player">
				    <div className="video-player__lead">
				      <div className="view-nodequeue-lists-news-featured-video-block">
				        <div>
				          <article className="media">
				            <div className="media__icon">
				              <div className="file-video">
				                <div className="media-wrap--16x9">
				                  <div className="media-youtube-video media-youtube-1">
				                    <iframe className="media-youtube-player" title="The palm cockatoo bangs out a beat to attract mates" src="https://www.youtube.com/embed/lKHmfkh7nJk?wmode=opaque&amp;modestbranding=1" frameBorder="0" allowFullScreen="">Video of The palm cockatoo bangs out a beat to attract mates</iframe>
				                  </div>
				                </div>
				              </div>
				            </div>
				            <div className="media__body">
				              <h2 className="media__headline"><a href="/video/cockatoos-use-tools-make-music">Cockatoos use tools to make music</a></h2>
				            </div>
				          </article>
				        </div>
				      </div>
				    </div>
				    <div className="video-player__playlist">
				      <div className="view-nodequeue-lists-news-most-popular-videos-block">
				        <ul className="item-list">
				          <li>
				            <article className="media media--var media--priority-2">
				              <div className="media__icon">
				                <a href="/video/crazy-faced-cats-don-t-win-adoption-game"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/still-no-logo.jpg?itok=oJRLfCJw" alt="cat face"/></a>  
				              </div>
				              <div className="media__body">
				                <h2 className="media__headline">
				                  <a href="/video/crazy-faced-cats-don-t-win-adoption-game">Crazy-faced cats don’t win the adoption game</a>
				                </h2>
				                <span property="dc:title" content="Crazy-faced cats don’t win the adoption game" className="rdf-meta element-hidden"></span>
				              </div>
				            </article>
				          </li>
				          <li>
				            <article className="media media--var media--priority-2">
				              <div className="media__icon">
				                <a href="/video/how-clumsy-galapagos-cormorant-lost-its-flight"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/images/Science_Cormorants_Logo.png?itok=hcbW3HDg" alt="Cormorant spreading wings"/></a>  
				              </div>
				              <div className="media__body">
				                <h2 className="media__headline">
				                  <a href="/video/how-clumsy-galapagos-cormorant-lost-its-flight">How the clumsy Galapagos cormorant lost its flight</a>
				                </h2>
				                <span property="dc:title" content="How the clumsy Galapagos cormorant lost its flight" className="rdf-meta element-hidden"></span>
				              </div>
				            </article>
				          </li>
				          <li>
				            <article className="media media--var media--priority-2">
				              <div className="media__icon">
				                <a href="/video/smashing-view-even-hammer-cant-break-glass"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/images/still-logo_2.jpg?itok=6GGyyjjj" alt="Drop of glass"/></a>  
				              </div>
				              <div className="media__body">
				                <h2 className="media__headline">
				                  <a href="/video/smashing-view-even-hammer-cant-break-glass">Smashing view: Even a hammer can't break this glass</a>
				                </h2>
				                <span property="dc:title" content="Smashing view: Even a hammer can't break this glass" className="rdf-meta element-hidden"></span>
				              </div>
				            </article>
				          </li>
				          <li>
				            <article className="media media--var media--priority-2">
				              <div className="media__icon">
				                <a href="/video/why-whales-grew-such-monster-sizes"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/images/Video%20thumb_3.jpg?itok=a_-Ke3Pn" alt="A whale"/></a>  
				              </div>
				              <div className="media__body">
				                <h2 className="media__headline">
				                  <a href="/video/why-whales-grew-such-monster-sizes">Why whales grew to such monster sizes</a>
				                </h2>
				                <span property="dc:title" content="Why whales grew to such monster sizes" className="rdf-meta element-hidden"></span>
				              </div>
				            </article>
				          </li>
				        </ul>
				      </div>
				    </div>
				  </div>
				</div>
			);
		}
}

export default Media;
