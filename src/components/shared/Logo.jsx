import React, { Component } from 'react';
const img = require('../../assets/images/logo-science.svg');

class Logo extends Component {

	render(){
		return(
			<div className="sticky-header__logo">
	      <a href="http://science.sciencemag.org" className="sticky-header__link">
	      	<img className="logo--science" alt="Science" title="Science" src={img}></img>
	      </a>
	    </div>
		);
	}
}


export default Logo;
