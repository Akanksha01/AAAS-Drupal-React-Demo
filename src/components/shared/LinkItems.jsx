import React, { Component } from 'react';
import PropTypes from 'prop-types';

class LinkItems extends Component {

	static propTypes = {
    redirectTo: PropTypes.string,
    title: PropTypes.string
  };

	render(){
		const {redirectTo, title} = this.props;

		return(
      <a href={redirectTo} >{title}</a>
		);
	}
}

export default LinkItems;
