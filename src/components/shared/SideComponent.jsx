import React, { Component } from 'react';

class SideComponent extends Component {

	render(){

		return(
			<div className="subprime--b">
				<div className="item-list">
				  <div className="box-standout box--from-the-mag">
				    <h2 className="subsection-title "><cite>Science</cite> Magazine</h2>
				    <div className="journal-summary">
				      <div className="journal-summary__cover">
				        <span><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/highwire/covers/sci/361/6404/-F1.medium.gif" width="346" height="440" alt="highwire"/></span>
				      </div>
				      <div className="journal-summary__details">
				        <p className="journal-summary__issue"><span>Vol 361</span>, <span>Issue 6404</span><br/>
				          24 August 2018    
				        </p>
				        <ul className="journal-summary__menu">
				          <li><a href="http://www.sciencemag.org/content/current">Table of Contents</a></li>
				          <li><a href="http://science.sciencemag.org/content/by/year">Past Issues</a></li>
				          <li><a href="https://pubs.aaas.org/Promo/promo_setup_rd.asp?dmc=P4XHU">Subscribe</a></li>
				        </ul>
				      </div>
				    </div>
				    <div className="layout-item">
				      <article className="media">
				        <h3 className="overline ">In Brief</h3>
				        <article className="">
				          <h2 className="media__headline"><a href="http://science.sciencemag.org/content/361/6404/734">News at a glance</a></h2>
				          <span property="dc:title" content="News at a glance" className="rdf-meta element-hidden"></span>      
				        </article>
				      </article>
				      <h3 className="overline">In Depth</h3>
				      <ul className="item-list">
				        <li>
				          <article className="media media--var media--priority-2">
				            <div className="media__icon">
				              <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/highwire/large/sci/361/6404/736-F1.large.jpg?itok=HvdBl6dr" alt="highwire"/>
				            </div>
				            <div className="media__body">
				              <h2 className="media__headline">
				                <a href="http://science.sciencemag.org/content/361/6404/736">Interplanetary small satellites come of age</a>
				              </h2>
				              <span property="dc:title" content="Interplanetary small satellites come of age" className="rdf-meta element-hidden"></span>
				              <p className="byline">
				                By <a href="/journal-author/eric-hand">Eric Hand</a><time>Aug. 24, 2018</time>
				              </p>
				            </div>
				          </article>
				        </li>
				        <li>
				          <article className="media media--var media--priority-2">
				            <div className="media__icon">
				              <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/highwire/large/sci/361/6404/737-F1.large.jpg?itok=cKsae5ur" alt="highwire"/>
				            </div>
				            <div className="media__body">
				              <h2 className="media__headline">
				                <a href="http://science.sciencemag.org/content/361/6404/737">Ancient DNA reveals tryst between extinct human species</a>
				              </h2>
				              <span property="dc:title" content="Ancient DNA reveals tryst between extinct human species" className="rdf-meta element-hidden"></span>
				              <p className="byline">
				                By <a href="/journal-author/gretchen-vogel">Gretchen Vogel</a><time>Aug. 24, 2018</time>
				              </p>
				            </div>
				          </article>
				        </li>
				        <li>
				          <article className="media media--var media--priority-2">
				            <div className="media__icon">
				              <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/highwire/large/sci/361/6404/738-F1.large.jpg?itok=RiUzHDAk" alt="highwire"/>
				            </div>
				            <div className="media__body">
				              <h2 className="media__headline">
				                <a href="http://science.sciencemag.org/content/361/6404/738">Project lifts the veil on life in the ocean's twilight zone</a>
				              </h2>
				              <span property="dc:title" content="Project lifts the veil on life in the ocean's twilight zone" className="rdf-meta element-hidden"></span>
				              <p className="byline">
				                By <a href="/journal-author/eli-kintisch">Eli Kintisch</a><time>Aug. 24, 2018</time>
				              </p>
				            </div>
				          </article>
				        </li>
				        <li>
				          <article className="media media--var media--priority-2">
				            <div className="media__icon">
				              <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/highwire/large/sci/361/6404/739-F1.large.jpg?itok=8EGj_NTW" alt="highwire"/>
				            </div>
				            <div className="media__body">
				              <h2 className="media__headline">
				                <a href="http://science.sciencemag.org/content/361/6404/739">Sticky glaciers slowed tempo of ice ages</a>
				              </h2>
				              <span property="dc:title" content="Sticky glaciers slowed tempo of ice ages" className="rdf-meta element-hidden"></span>
				              <p className="byline">
				                By <a href="/journal-author/paul-voosen">Paul Voosen</a><time>Aug. 24, 2018</time>
				              </p>
				            </div>
				          </article>
				        </li>
				        <li>
				          <article className="media media--var media--priority-2">
				            <div className="media__icon">
				              <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/highwire/large/sci/361/6404/740-F1.large.jpg?itok=4HnH0eML" alt="highwire"/>
				            </div>
				            <div className="media__body">
				              <h2 className="media__headline">
				                <a href="http://science.sciencemag.org/content/361/6404/740">Monthly shots may replace daily anti-HIV pills</a>
				              </h2>
				              <span property="dc:title" content="Monthly shots may replace daily anti-HIV pills" className="rdf-meta element-hidden"></span>
				              <p className="byline">
				                By <a href="/journal-author/jon-cohen">Jon Cohen</a><time>Aug. 24, 2018</time>
				              </p>
				            </div>
				          </article>
				        </li>
				        <li>
				          <article className="media media--var media--priority-2">
				            <div className="media__icon">
				              <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/highwire/large/sci/361/6404/741-F1.large.jpg?itok=sozmlo5t" alt="highwire"/>
				            </div>
				            <div className="media__body">
				              <h2 className="media__headline">
				                <a href="http://science.sciencemag.org/content/361/6404/741">Arrival of deadly pig disease could spell disaster for China</a>
				              </h2>
				              <span property="dc:title" content="Arrival of deadly pig disease could spell disaster for China" className="rdf-meta element-hidden"></span>
				              <p className="byline">
				                By <a href="/journal-author/dennis-normile">Dennis Normile</a><time>Aug. 24, 2018</time>
				              </p>
				            </div>
				          </article>
				        </li>
				      </ul>
				      <h3 className="overline">Features</h3>
				      <ul className="item-list">
				        <li>
				          <article className="media headline-list__outer-lead" about="/feature/daring-hope" typeof="sioc:Item foaf:Document">
				            <div className="media__icon">
				              <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/article_main_medium/public/highwire/large/sci/361/6404/742-F1.large.jpg?itok=xMMZVwb3" alt="highwire"/>
				            </div>
				            <div className="media__body">
				              <h2 className="media__headline"><a href="http://science.sciencemag.org/content/361/6404/742">Daring to hope</a></h2>
				              <span property="dc:title" content="Daring to hope" className="rdf-meta element-hidden"></span>              
				              <p className="byline">
				                By <a href="/journal-author/meredith-wadman">Meredith Wadman</a><time>Aug. 24, 2018</time>
				              </p>
				            </div>
				          </article>
				        </li>
				      </ul>
				      <p><a href="http://www.sciencemag.org/content/current" className="btn--block">More from this issue</a></p>
				    </div>
				  </div>
				</div>
			</div>
			);
		}
}

export default SideComponent;
