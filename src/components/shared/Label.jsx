import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Label extends Component {

	static propTypes = {
    className: PropTypes.string,
    htmlFor: PropTypes.string,
    fontClassName: PropTypes.string
  };


	render(){
		const {className, htmlFor, fontClassName} = this.props;

		return(
			<label className={className} htmlFor={htmlFor}>
				<i className={fontClassName}></i>
			</label>
		);
	}
}

export default Label;
