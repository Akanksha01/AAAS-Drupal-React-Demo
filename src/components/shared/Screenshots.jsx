import React, { Component } from 'react';

class Screenshots extends Component {

  render(){

    return(
      <div className="layout-item">
        <div className="view-article-lists-block-14">
          <h2 className="title subsection-title">ScienceShots</h2>
          <ul className="media-grid media-grid--3">
            <li>
              <article className="media media" about="/news/2018/08/how-oobleck-might-one-day-prevent-airline-fires" typeof="sioc:Item foaf:Document">
                <div className="media__icon">
                  <a href="/news/2018/08/how-oobleck-might-one-day-prevent-airline-fires"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/airplane_16x9.jpg?itok=g_0GAUav" alt="mergency vehicles near a smoking cargo airplane"/></a>  
                </div>
                <div className="media__body">
                  <h2 className="media__headline"><a href="/news/2018/08/how-oobleck-might-one-day-prevent-airline-fires">How a liquid that acts like a solid could prevent plane fires</a></h2>
                  <span property="dc:title" content="How a liquid that acts like a solid could prevent plane fires" className="rdf-meta element-hidden"></span>              
                  <p className="byline">
                    By <a href="/author/randall-hyman">Randall Hyman</a><time>Aug. 22, 2018</time>
                  </p>
                </div>
              </article>
            </li>
            <li>
              <article className="media media" about="/news/2018/08/parasitic-love-vine-sucking-life-out-freeloading-wasps" typeof="sioc:Item foaf:Document">
                <div className="media__icon">
                  <a href="/news/2018/08/parasitic-love-vine-sucking-life-out-freeloading-wasps"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/parasite_16x9.jpg?itok=gh0fiiej" alt="Love vine feeding on gall wasp"/></a>  
                </div>
                <div className="media__body">
                  <h2 className="media__headline"><a href="/news/2018/08/parasitic-love-vine-sucking-life-out-freeloading-wasps">This parasitic ‘love vine’ is sucking the life out of freeloading wasps</a></h2>
                  <span property="dc:title" content="This parasitic ‘love vine’ is sucking the life out of freeloading wasps" className="rdf-meta element-hidden"></span>              
                  <p className="byline">
                    By <a href="/author/david-shultz">David Shultz</a><time>Aug. 21, 2018</time>
                  </p>
                </div>
              </article>
            </li>
            <li>
              <article className="media media" about="/news/2018/08/bright-purple-ribbon-named-steve-entirely-new-celestial-phenomenon" typeof="sioc:Item foaf:Document">
                <div className="media__icon">
                  <a href="/news/2018/08/bright-purple-ribbon-named-steve-entirely-new-celestial-phenomenon"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/STEVE_16x9_0.jpg?itok=to_a74n1" alt="STEVE and the milky way over Childs Lake"/></a>  
                </div>
                <div className="media__body">
                  <h2 className="media__headline"><a href="/news/2018/08/bright-purple-ribbon-named-steve-entirely-new-celestial-phenomenon">This bright purple ribbon—named STEVE—is an entirely new celestial phenomenon</a></h2>
                  <span property="dc:title" content="This bright purple ribbon—named STEVE—is an entirely new celestial phenomenon" className="rdf-meta element-hidden"></span>              
                  <p className="byline">
                    By <a href="/author/katherine-kornei">Katherine Kornei</a><time>Aug. 21, 2018</time>
                  </p>
                </div>
              </article>
            </li>
            <li>
              <article className="media media" about="/news/2018/08/why-does-ai-stink-certain-video-games-researchers-made-one-play-ms-pac-man-find-out" typeof="sioc:Item foaf:Document">
                <div className="media__icon">
                  <a href="/news/2018/08/why-does-ai-stink-certain-video-games-researchers-made-one-play-ms-pac-man-find-out"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/AW3H43-1280x720.jpg?itok=IlqLxIzW" alt="Atari game screenshot "/></a>  
                </div>
                <div className="media__body">
                  <h2 className="media__headline"><a href="/news/2018/08/why-does-ai-stink-certain-video-games-researchers-made-one-play-ms-pac-man-find-out">Why does AI stink at certain video games? Researchers made one play <i>Ms. Pac-Man</i> to find out</a></h2>
                  <span property="dc:title" content="Why does AI stink at certain video games? Researchers made one play <i>Ms. Pac-Man</i> to find out" className="rdf-meta element-hidden"></span>              
                  <p className="byline">
                    By <a href="/author/matthew-hutson">Matthew Hutson</a><time>Aug. 17, 2018</time>
                  </p>
                </div>
              </article>
            </li>
            <li>
              <article className="media media" about="/news/2018/08/being-watched-cranky-robot-might-help-you-focus" typeof="sioc:Item foaf:Document">
                <div className="media__icon">
                  <a href="/news/2018/08/being-watched-cranky-robot-might-help-you-focus"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/robots081518.jpg?itok=H_cXQ5jm" alt="illustration of robot and human side-by-side at computer monitors"/></a>  
                </div>
                <div className="media__body">
                  <h2 className="media__headline"><a href="/news/2018/08/being-watched-cranky-robot-might-help-you-focus">Being watched by a cranky robot might help you focus</a></h2>
                  <span property="dc:title" content="Being watched by a cranky robot might help you focus" className="rdf-meta element-hidden"></span>              
                  <p className="byline">
                    By <a href="/author/frankie-schembri">Frankie Schembri</a><time>Aug. 15, 2018</time>
                  </p>
                </div>
              </article>
            </li>
            <li>
              <article className="media media" about="/news/2018/08/people-might-have-bred-tropical-macaws-desert-1000-years-ago" typeof="sioc:Item foaf:Document">
                <div className="media__icon">
                  <a href="/news/2018/08/people-might-have-bred-tropical-macaws-desert-1000-years-ago"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/macaw_16x9.jpg?itok=YOiQcPfP" alt="scarlet macaw flying"/></a>  
                </div>
                <div className="media__body">
                  <h2 className="media__headline"><a href="/news/2018/08/people-might-have-bred-tropical-macaws-desert-1000-years-ago">People might have bred tropical macaws in the desert 1000 years ago</a></h2>
                  <span property="dc:title" content="People might have bred tropical macaws in the desert 1000 years ago" className="rdf-meta element-hidden"></span>              
                  <p className="byline">
                    By <a href="/author/lizzie-wade">Lizzie Wade</a><time>Aug. 13, 2018</time>
                  </p>
                </div>
              </article>
            </li>
          </ul>
        </div>
      </div>
      );
    }
}

export default Screenshots;
