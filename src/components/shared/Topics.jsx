import React, { Component } from 'react';

class Topics extends Component {

  render(){

    return(
      <div className="layout-item">
        <div className="view-nodequeue-3-block">
          <h2 className="subsection-title heading--block">Topics <a href="/topics">View All Topics</a></h2>
          <div className="homepage__topics categories__grid">
            <div>
              <ul className="headline-list">
                <li>
                  <article className="media media--flip">
                    <div className="media__icon">
                      <a href="/topic/memory"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/Memory.jpg?itok=V4H-32p4" alt="grid"/></a>  
                    </div>
                    <div className="media__body">
                      <h2 className="media__headline"><a href="/topic/memory">Memory</a></h2>
                      <span property="dc:title" content="Memory" className="rdf-meta element-hidden"></span>        
                    </div>
                  </article>
                </li>
                <li>  
                  <a href="/news/2016/05/tau-protein-not-amyloid-may-be-key-driver-alzheimer-s-symptoms">Tau protein—not amyloid—may be key driver of Alzheimer’s symptoms</a>  
                </li>
                <li>  
                  <a href="/news/2016/04/do-sleeping-dragons-dream">Do sleeping dragons dream?</a>  
                </li>
                <li>  
                  <a href="/news/2016/03/alzheimer-s-may-be-caused-haywire-immune-system-eating-brain-connections">Alzheimer’s may be caused by haywire immune system eating brain connections</a>  
                </li>
              </ul>
            </div>
            <div>
              <ul className="headline-list">
                <li>
                  <article className="media media--flip">
                    <div className="media__icon">
                      <a href="/topic/cancer"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/images/Cancer_16x9.jpg?itok=JbOyqPWq" alt="cancer"/></a>  
                    </div>
                    <div className="media__body">
                      <h2 className="media__headline"><a href="/topic/cancer">Cancer</a></h2>
                      <span property="dc:title" content="Cancer" className="rdf-meta element-hidden"></span>        
                    </div>
                  </article>
                </li>
                <li>  
                  <a href="/news/2017/02/reprogrammed-skin-cells-shrink-brain-tumors-mice">Reprogrammed skin cells shrink brain tumors in mice</a>  
                </li>
                <li>  
                  <a href="/news/2017/01/nanoparticles-awaken-immune-cells-fight-cancer">Nanoparticles awaken immune cells to fight cancer</a>  
                </li>
                <li>  
                  <a href="/news/2016/10/biden-s-moonshot-cancer-plan-calls-more-data-sharing">Biden’s moonshot cancer plan calls for more data sharing</a>  
                </li>
              </ul>
            </div>
            <div>
              <ul className="headline-list">
                <li>
                  <article className="media media--flip">
                    <div className="media__icon">
                      <a href="/topic/crispr"><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/images/crispr.jpg?itok=KursAu_y" title="V. Altounian, Science" alt="science"/></a>  
                    </div>
                    <div className="media__body">
                      <h2 className="media__headline"><a href="/topic/crispr">CRISPR</a></h2>
                      <span property="dc:title" content="CRISPR" className="rdf-meta element-hidden"></span>        
                    </div>
                  </article>
                </li>
                <li>  
                  <a href="/news/2016/12/stopping-crispr-s-genome-editing-scissors-snipping-out-control">Stopping CRISPR’s genome-editing scissors from snipping out of control</a>  
                </li>
                <li>  
                  <a href="/news/2016/12/crispr-patent-hearing-produces-no-clear-winner-only-soft-signals">CRISPR patent hearing produces no clear winner, only ‘soft signals’</a>  
                </li>
                <li>  
                  <a href="/news/2016/09/did-swedish-researcher-eat-first-crispr-meal-ever-served">Did a Swedish researcher eat the first CRISPR meal ever served?</a>  
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      );
    }
}

export default Topics;
