import React, { Component } from 'react';

class ScienceInsider extends Component {

  render(){

    return(
      <div className="layout-item">
        <div className="view-article-lists-block-2">
          <h2 className="subsection-title "><cite>Science</cite>Insider</h2>
          <ul className="item-list">
            <li>
              <article className="media media--var media--priority-2">
                <div className="media__icon">
                  <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/AP_17255765404720-1280x720.jpg?itok=5KUYAlIt" alt="Kelvin Droegemeier, Oklahoma Secretary of Science and Technology, is pictured in Oklahoma City, Tuesday, Sept. 12, 2017. "/>  
                </div>
                <div className="media__body">
                  <h2 className="media__headline">
                    <a href="/news/2018/08/white-house-science-nominee-ducks-chance-refute-climate-skeptic-senate-confirmation">White House science nominee ducks chance to refute climate skeptic at Senate confirmation hearing</a>
                  </h2>
                  <span property="dc:title" content="White House science nominee ducks chance to refute climate skeptic at Senate confirmation hearing" className="rdf-meta element-hidden"></span>
                  <p className="byline">
                    By <a href="/author/jeffrey-mervis">Jeffrey Mervis</a><time>Aug. 23, 2018</time>
                  </p>
                </div>
              </article>
            </li>
            <li>
              <article className="media media--var media--priority-2">
                <div className="media__icon">
                  <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/crop%20duster.jpg?itok=rfZqq0Pg" alt="A black and yellow crop dusting plane sprays liquid on a field of crops."/>  
                </div>
                <div className="media__body">
                  <h2 className="media__headline">
                    <a href="/news/2018/08/battle-over-pesticide-ban-trump-s-epa-aims-undermine-science">In battle over pesticide ban, Trump’s EPA aims to undermine the science</a>
                  </h2>
                  <span property="dc:title" content="In battle over pesticide ban, Trump’s EPA aims to undermine the science" className="rdf-meta element-hidden"></span>
                  <p className="byline">
                    By <a href="/author/corbin-hiar-ee-news">Corbin Hiar, E&amp;E News</a><time>Aug. 23, 2018</time>
                  </p>
                </div>
              </article>
            </li>
            <li>
              <article className="media media--var media--priority-2">
                <div className="media__icon">
                  <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/sonde%20launch.jpg?itok=T-6Rb7mG" alt="scientific balloon launch in Greenland"/>  
                </div>
                <div className="media__body">
                  <h2 className="media__headline">
                    <a href="/news/2018/08/here-are-10-topics-senators-could-ask-trump-s-science-adviser-nominee-address">Here are 10 topics senators could ask Trump’s science adviser nominee to address</a>
                  </h2>
                  <span property="dc:title" content="Here are 10 topics senators could ask Trump’s science adviser nominee to address" className="rdf-meta element-hidden"></span>
                  <p className="byline">
                    By <a href="/author/jeffrey-mervis">Jeffrey Mervis</a><time>Aug. 21, 2018</time>
                  </p>
                </div>
              </article>
            </li>
            <li>
              <article className="media media--var media--priority-2">
                <div className="media__icon">
                  <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/720Donald_Trump_%2829496131773%29.jpg?itok=xp2ApAd2" alt="President Donald Trump"/>  
                </div>
                <div className="media__body">
                  <h2 className="media__headline">
                    <a href="/news/2018/08/trump-releases-plan-roll-back-obama-administration-climate-rules-aimed-power-plants">Trump releases plan to roll back Obama administration climate rules aimed at power plants </a>
                  </h2>
                  <span property="dc:title" content="Trump releases plan to roll back Obama administration climate rules aimed at power plants " className="rdf-meta element-hidden"></span>
                  <p className="byline">
                    By <a href="/author/zack-colman-ee-news">Zack Colman, E&amp;E News</a><time>Aug. 21, 2018</time>
                  </p>
                </div>
              </article>
            </li>
            <li>
              <article className="media media--var media--priority-2">
                <div className="media__icon">
                  <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/AP_17255765404720-1280x720.jpg?itok=5KUYAlIt" alt="Kelvin Droegemeier, Oklahoma Secretary of Science and Technology, is pictured in Oklahoma City, Tuesday, Sept. 12, 2017. "/>  
                </div>
                <div className="media__body">
                  <h2 className="media__headline">
                    <a href="/news/2018/08/what-does-trump-s-pick-science-adviser-think-about-climate-science-2014-talk-offers">What does Trump’s pick for science adviser think about climate science? A 2014 talk offers clues</a>
                  </h2>
                  <span property="dc:title" content="What does Trump’s pick for science adviser think about climate science? A 2014 talk offers clues" className="rdf-meta element-hidden"></span>
                  <p className="byline">
                    By <a href="/author/jeffrey-mervis">Jeffrey Mervis</a><time>Aug. 20, 2018</time>
                  </p>
                </div>
              </article>
            </li>
          </ul>
          <p><a href="/news/scienceinsider" className="btn--block">More <i>Science</i>Insider</a></p>
        </div>
      </div>
      );
    }
}

export default ScienceInsider;
