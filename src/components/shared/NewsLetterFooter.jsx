import React, { Component } from 'react';

class NewsLetterFooter extends Component {

  render(){
    return(
      <div className="row row--alt" lang="en">
        <div className="container container--promo">
          <div className="primary">
            <a href="# " className="heading--block">
              <h2 className="subsection-title">Science</h2>
            </a>
            <div className="primary__main promo-foot--mag">
              <div className="promo-foot--current">
                <div className="view-footer-promo-block-issue-footer">
                  <div>
                    <h3>24 August 2018</h3>
                    <p className="promo-foot__meta">
                      Vol 361, Issue 6404  </p>
                    <a href="# ">
                      <img className="promo-foot__cover" alt="Magazine Cover" src="http://www.sciencemag.org/sites/default/files/styles/medium/public/highwire/covers/sci/361/6404/-F1.medium.gif?itok=T6nAZxtW"/>
                    </a>
                  </div>
                </div>
              </div>
              <div className="promo-foot--heds">
                <div className="view-footer-promo-block-footer-news">
                  <ul className="media-grid media-grid--2 media-grid--minor">
                    <li>  
                      <h3 className="overline overline--priority-2">Medicine/Diseases</h3>    
                      <a href="http://science.sciencemag.org/content/361/6404/742">Daring to hope</a>  
                    </li>
                    <li>  
                      <h3 className="overline overline--priority-2">Epidemiology</h3>    
                      <a href="http://science.sciencemag.org/content/361/6404/741">Arrival of deadly pig disease could spell disaster for China</a> 
                    </li>
                    <li>  
                      <h3 className="overline overline--priority-2">Epidemiology</h3>    
                      <a href="http://science.sciencemag.org/content/361/6404/740">Monthly shots may replace daily anti-HIV pills</a>
                    </li>
                    <li>  
                      <h3 className="overline overline--priority-2">Ecology</h3>    
                      <a href="http://science.sciencemag.org/content/361/6404/739">Sticky glaciers slowed tempo of ice ages</a>  
                    </li>
                    <li>  
                      <h3 className="overline overline--priority-2">Oceanography</h3>    
                      <a href="http://science.sciencemag.org/content/361/6404/738">Project lifts the veil on life in the ocean's twilight zone</a>
                    </li>
                    <li>  
                      <h3 className="overline overline--priority-2">Anthropology</h3>    
                      <a href="http://science.sciencemag.org/content/361/6404/737">Ancient DNA reveals tryst between extinct human species</a>
                    </li>
                  </ul>
                  <p><a href="http://www.sciencemag.org/content/current" className="btn--cta btn--cta--small">Table of Contents</a></p>
                </div>
              </div>
            </div>
            <div className="primary__secondary" id="aaas-util-foot-1">
              <div className="promo-foot--subscribe">
                <h3>Subscribe Today</h3>
                <p className="priority-2">Receive a year subscription to <cite>Science</cite> plus access to exclusive AAAS member resources, opportunities, and benefits.</p>
                <p className="priority-2">
                  <label htmlFor="firstname" className="element-invisible">First Name</label>
                  <input type="text" name="firstname" id="firstname" placeholder="First Name"/>
                </p>

                <p className="priority-2">
                  <label htmlFor="lastname" className="element-invisible">Last Name</label>
                  <input type="text" name="lastname" id="lastname" placeholder="Last Name"/>
                </p>

                <p className="priority-2">
                  <label htmlFor="email" className="element-invisible">Email Address</label>
                  <input type="email" name="email" id="email" placeholder="Email Address"/>
                </p>

                <p><button type="submit" className="btn--cta btn--cta--small">Subscribe Today</button></p>
                <input type="hidden" name="__successPage" id="__successPage" value="https://pubs.aaas.org/Promo/promo_setup_rd.asp?dmc=P5XPZ"/>
                <input type="hidden" name="__errorPage" id="__errorPage" value="https://pubs.aaas.org/Promo/promo_setup_rd.asp?dmc=P5XPZ"/>
                <input type="hidden" name="__contextName" id="__contextName" value="FormPost"/>
                <input type="hidden" name="__executionContext" id="__executionContext" value="Post"/>
              </div>
            </div>
          </div>
          <div className="secondary" id="aaas-util-foot-2">
            <div className="promo--newsletters">
              <h2 className="subsection-title">Get Our Newsletters </h2>
              <p className="priority-2">Receive emails from <cite>Science</cite>. <a href="//www.sciencemag.org/subscribe/get-our-newsletters">See full list</a></p>
              <div id="newsletter-footer-checkboxes">
                <p className="priority-2"><label><input checked="checked" name="33 Science Magazine TOC" type="checkbox" value="true" readOnly/> <cite>Science</cite> Table of Contents</label></p>
                <p className="priority-2"><label><input checked="checked" name="35 Science Latest News and Headlines" type="checkbox" value="true" readOnly/> <cite>Science</cite> Daily News </label></p>
                <p className="priority-2"><label><input checked="checked" name="36 News from Science Weekly Headlines" type="checkbox" value="true" readOnly/> <cite>Science</cite> News This Week</label></p>
                <p className="priority-2"><label><input checked="checked" name="31 Editors Choice" type="checkbox" value="true" readOnly/> <cite>Science</cite> Editor's Choice</label></p>
                <p className="priority-2"><label><input checked="checked" name="32 Science Express Notification" type="checkbox" value="true" readOnly/> First Release Notification</label></p>
                <p className="priority-2"><label><input checked="checked" name="44 Job Seeker Newsletter" type="checkbox" value="true" readOnly/> <cite>Science</cite> Careers Job Seeker</label></p>
              </div>
              <label htmlFor="newsletter-footer-country" className="element-invisible">Country</label>
              <select style={{"width" : "100%", "fontSize": ".825rem", "padding" : "0.25rem"}} className="inpt" id="newsletter-footer-country" name="Country">
                  <option defaultValue="" value="">Country *</option>
                  <option value="AF">Afghanistan</option>
                  <option value="AX">Aland Islands</option>
                  <option value="AL">Albania</option>
                  <option value="DZ">Algeria</option>
                  <option value="AD">Andorra</option>
                  <option value="AO">Angola</option>
                  <option value="AI">Anguilla</option>
                  <option value="AQ">Antarctica</option>
                  <option value="AG">Antigua and Barbuda</option>
                  <option value="AR">Argentina</option>
                  <option value="AM">Armenia</option>
                  <option value="AW">Aruba</option>
                  <option value="AU">Australia</option>
                  <option value="AT">Austria</option>
                  <option value="AZ">Azerbaijan</option>
                  <option value="BS">Bahamas</option>
                  <option value="BH">Bahrain</option>
                  <option value="BD">Bangladesh</option>
                  <option value="BB">Barbados</option>
                  <option value="BY">Belarus</option>
                  <option value="BE">Belgium</option>
                  <option value="BZ">Belize</option>
                  <option value="BJ">Benin</option>
                  <option value="BM">Bermuda</option>
                  <option value="BT">Bhutan</option>
                  <option value="BO">Bolivia, Plurinational State of</option>
                  <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                  <option value="BA">Bosnia and Herzegovina</option>
                  <option value="BW">Botswana</option>
                  <option value="BV">Bouvet Island</option>
                  <option value="BR">Brazil</option>
                  <option value="IO">British Indian Ocean Territory</option>
                  <option value="BN">Brunei Darussalam</option>
                  <option value="BG">Bulgaria</option>
                  <option value="BF">Burkina Faso</option>
                  <option value="BI">Burundi</option>
                  <option value="KH">Cambodia</option>
                  <option value="CM">Cameroon</option>
                  <option value="CA">Canada</option>
                  <option value="CV">Cape Verde</option>
                  <option value="KY">Cayman Islands</option>
                  <option value="CF">Central African Republic</option>
                  <option value="TD">Chad</option>
                  <option value="CL">Chile</option>
                  <option value="CN">China</option>
                  <option value="CX">Christmas Island</option>
                  <option value="CC">Cocos (Keeling) Islands</option>
                  <option value="CO">Colombia</option>
                  <option value="KM">Comoros</option>
                  <option value="CG">Congo</option>
                  <option value="CD">Congo, the Democratic Republic of the</option>
                  <option value="CK">Cook Islands</option>
                  <option value="CR">Costa Rica</option>
                  <option value="CI">Cote d’Ivoire</option>
                  <option value="HR">Croatia</option>
                  <option value="CU">Cuba</option>
                  <option value="CW">Curaçao</option>
                  <option value="CY">Cyprus</option>
                  <option value="CZ">Czech Republic</option>
                  <option value="DK">Denmark</option>
                  <option value="DJ">Djibouti</option>
                  <option value="DM">Dominica</option>
                  <option value="DO">Dominican Republic</option>
                  <option value="EC">Ecuador</option>
                  <option value="EG">Egypt</option>
                  <option value="SV">El Salvador</option>
                  <option value="GQ">Equatorial Guinea</option>
                  <option value="ER">Eritrea</option>
                  <option value="EE">Estonia</option>
                  <option value="ET">Ethiopia</option>
                  <option value="FK">Falkland Islands (Malvinas)</option>
                  <option value="FO">Faroe Islands</option>
                  <option value="FJ">Fiji</option>
                  <option value="FI">Finland</option>
                  <option value="FR">France</option>
                  <option value="GF">French Guiana</option>
                  <option value="PF">French Polynesia</option>
                  <option value="TF">French Southern Territories</option>
                  <option value="GA">Gabon</option>
                  <option value="GM">Gambia</option>
                  <option value="GE">Georgia</option>
                  <option value="DE">Germany</option>
                  <option value="GH">Ghana</option>
                  <option value="GI">Gibraltar</option>
                  <option value="GR">Greece</option>
                  <option value="GL">Greenland</option>
                  <option value="GD">Grenada</option>
                  <option value="GP">Guadeloupe</option>
                  <option value="GT">Guatemala</option>
                  <option value="GG">Guernsey</option>
                  <option value="GN">Guinea</option>
                  <option value="GW">Guinea-Bissau</option>
                  <option value="GY">Guyana</option>
                  <option value="HT">Haiti</option>
                  <option value="HM">Heard Island and McDonald Islands</option>
                  <option value="VA">Holy See (Vatican City State)</option>
                  <option value="HN">Honduras</option>
                  <option value="HU">Hungary</option>
                  <option value="IS">Iceland</option>
                  <option value="IN">India</option>
                  <option value="ID">Indonesia</option>
                  <option value="IR">Iran, Islamic Republic of</option>
                  <option value="IQ">Iraq</option>
                  <option value="IE">Ireland</option>
                  <option value="IM">Isle of Man</option>
                  <option value="IL">Israel</option>
                  <option value="IT">Italy</option>
                  <option value="JM">Jamaica</option>
                  <option value="JP">Japan</option>
                  <option value="JE">Jersey</option>
                  <option value="JO">Jordan</option>
                  <option value="KZ">Kazakhstan</option>
                  <option value="KE">Kenya</option>
                  <option value="KI">Kiribati</option>
                  <option value="KP">Korea, Democratic People’s Republic of</option>
                  <option value="KR">Korea, Republic of</option>
                  <option value="KW">Kuwait</option>
                  <option value="KG">Kyrgyzstan</option>
                  <option value="LA">Lao People’s Democratic Republic</option>
                  <option value="LV">Latvia</option>
                  <option value="LB">Lebanon</option>
                  <option value="LS">Lesotho</option>
                  <option value="LR">Liberia</option>
                  <option value="LY">Libyan Arab Jamahiriya</option>
                  <option value="LI">Liechtenstein</option>
                  <option value="LT">Lithuania</option>
                  <option value="LU">Luxembourg</option>
                  <option value="MO">Macao</option>
                  <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                  <option value="MG">Madagascar</option>
                  <option value="MW">Malawi</option>
                  <option value="MY">Malaysia</option>
                  <option value="MV">Maldives</option>
                  <option value="ML">Mali</option>
                  <option value="MT">Malta</option>
                  <option value="MQ">Martinique</option>
                  <option value="MR">Mauritania</option>
                  <option value="MU">Mauritius</option>
                  <option value="YT">Mayotte</option>
                  <option value="MX">Mexico</option>
                  <option value="MD">Moldova, Republic of</option>
                  <option value="MC">Monaco</option>
                  <option value="MN">Mongolia</option>
                  <option value="ME">Montenegro</option>
                  <option value="MS">Montserrat</option>
                  <option value="MA">Morocco</option>
                  <option value="MZ">Mozambique</option>
                  <option value="MM">Myanmar</option>
                  <option value="NA">Namibia</option>
                  <option value="NR">Nauru</option>
                  <option value="NP">Nepal</option>
                  <option value="NL">Netherlands</option>
                  <option value="NC">New Caledonia</option>
                  <option value="NZ">New Zealand</option>
                  <option value="NI">Nicaragua</option>
                  <option value="NE">Niger</option>
                  <option value="NG">Nigeria</option>
                  <option value="NU">Niue</option>
                  <option value="NF">Norfolk Island</option>
                  <option value="NO">Norway</option>
                  <option value="OM">Oman</option>
                  <option value="PK">Pakistan</option>
                  <option value="PS">Palestine</option>
                  <option value="PA">Panama</option>
                  <option value="PG">Papua New Guinea</option>
                  <option value="PY">Paraguay</option>
                  <option value="PE">Peru</option>
                  <option value="PH">Philippines</option>
                  <option value="PN">Pitcairn</option>
                  <option value="PL">Poland</option>
                  <option value="PT">Portugal</option>
                  <option value="QA">Qatar</option>
                  <option value="RE">Reunion</option>
                  <option value="RO">Romania</option>
                  <option value="RU">Russian Federation</option>
                  <option value="RW">Rwanda</option>
                  <option value="BL">Saint Barthélemy</option>
                  <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                  <option value="KN">Saint Kitts and Nevis</option>
                  <option value="LC">Saint Lucia</option>
                  <option value="MF">Saint Martin (French part)</option>
                  <option value="PM">Saint Pierre and Miquelon</option>
                  <option value="VC">Saint Vincent and the Grenadines</option>
                  <option value="WS">Samoa</option>
                  <option value="SM">San Marino</option>
                  <option value="ST">Sao Tome and Principe</option>
                  <option value="SA">Saudi Arabia</option>
                  <option value="SN">Senegal</option>
                  <option value="RS">Serbia</option>
                  <option value="SC">Seychelles</option>
                  <option value="SL">Sierra Leone</option>
                  <option value="SG">Singapore</option>
                  <option value="SX">Sint Maarten (Dutch part)</option>
                  <option value="SK">Slovakia</option>
                  <option value="SI">Slovenia</option>
                  <option value="SB">Solomon Islands</option>
                  <option value="SO">Somalia</option>
                  <option value="ZA">South Africa</option>
                  <option value="GS">South Georgia and the South Sandwich Islands</option>
                  <option value="SS">South Sudan</option>
                  <option value="ES">Spain</option>
                  <option value="LK">Sri Lanka</option>
                  <option value="SD">Sudan</option>
                  <option value="SR">Suriname</option>
                  <option value="SJ">Svalbard and Jan Mayen</option>
                  <option value="SZ">Swaziland</option>
                  <option value="SE">Sweden</option>
                  <option value="CH">Switzerland</option>
                  <option value="SY">Syrian Arab Republic</option>
                  <option value="TW">Taiwan</option>
                  <option value="TJ">Tajikistan</option>
                  <option value="TZ">Tanzania, United Republic of</option>
                  <option value="TH">Thailand</option>
                  <option value="TL">Timor-Leste</option>
                  <option value="TG">Togo</option>
                  <option value="TK">Tokelau</option>
                  <option value="TO">Tonga</option>
                  <option value="TT">Trinidad and Tobago</option>
                  <option value="TN">Tunisia</option>
                  <option value="TR">Turkey</option>
                  <option value="TM">Turkmenistan</option>
                  <option value="TC">Turks and Caicos Islands</option>
                  <option value="TV">Tuvalu</option>
                  <option value="UG">Uganda</option>
                  <option value="UA">Ukraine</option>
                  <option value="AE">United Arab Emirates</option>
                  <option value="GB">United Kingdom</option>
                  <option value="US">United States</option>
                  <option value="UY">Uruguay</option>
                  <option value="UZ">Uzbekistan</option>
                  <option value="VU">Vanuatu</option>
                  <option value="VE">Venezuela, Bolivarian Republic of</option>
                  <option value="VN">Vietnam</option>
                  <option value="VG">Virgin Islands, British</option>
                  <option value="WF">Wallis and Futuna</option>
                  <option value="EH">Western Sahara</option>
                  <option value="YE">Yemen</option>
                  <option value="ZM">Zambia</option>
                  <option value="ZW">Zimbabwe</option>
                </select>
                <p className="priority-2">
                  <label htmlFor="newsletter-footer-email" className="element-invisible">Email</label>
                  <input id="newsletter-footer-email" maxLength="254" name="EmailAddress" size="35" type="email" value="" placeholder="Email address *"/>
                </p>
                <p className="priority-2">
                  <label><input id="newsletter-footer-consent-third-party" name="Consent_ThirdPartyEmails" type="checkbox" value="true"/> I agree to receive emails from AAAS/<cite>Science</cite> and <cite>Science</cite> advertisers, including information on products, services, and special offers which may include but are not limited to news, career information, &amp; upcoming events.</label>
                </p>
                <p className="priority-2">Click to view the <a href="//www.sciencemag.org/about/privacy-policy" target="_blank" rel="noopener noreferrer">Privacy Policy</a>.</p>
                <p><label htmlFor="newsletter-footer-submit" className="element-invisible">Sign up today</label>
                <button type="submit" id="newsletter-footer-submit" className="btn--cta btn--cta--small" disabled="true">Sign up today</button></p>
                <p className="priority-3">Required fields are indicated by an asterisk (*)</p>
                <input id="__successPage" name="__successPage" type="hidden" value="//www.sciencemag.org/subscribe/thank-you"/>
                <input id="__errorPage" name="__errorPage" type="hidden" value="//www.sciencemag.org/subscribe/oops"/>
                <input type="hidden" id="newsletter-footer-consent-first-party" name="Consent_FirstPartyEmails" value=""/>
                <input type="hidden" id="newsletter-footer-consent-first-party-timestamp" name="Consent_FirstPartyEmails_Timestamp" value=""/>
                <input type="hidden" id="newsletter-footer-consent-first-party-origin" name="Consent_FirstPartyEmails_Origin" value=""/>
                <input type="hidden" id="newsletter-footer-consent-third-party-timestamp" name="Consent_ThirdPartyEmails_Timestamp" value=""/>
                <input type="hidden" id="newsletter-footer-consent-third-party-origin" name="Consent_ThirdPartyEmails_Origin" value=""/>
                <input type="hidden" id="newsletter-footer-consent-editorial" name="Consent_EditorialContentEmails" value="true"/>
                <input type="hidden" id="newsletter-footer-consent-editorial-timestamp" name="Consent_EditorialContentEmails_Timestamp" value=""/>
                <input type="hidden" id="newsletter-footer-consent-editorial-origin" name="Consent_EditorialContentEmails_Origin" value="NL_Signup_Footer"/>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default NewsLetterFooter;

