import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Input extends Component {

	static propTypes = {
    title: PropTypes.string,
    className: PropTypes.string,
    placeholder: PropTypes.string,
    type: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string,
    size: PropTypes.string,
    maxLength: PropTypes.string
  };

	render(){
		const {title, className, placeholder, type, id, name, value, size, maxLength} = this.props;

		return(
			<input title={title} className={className} placeholder={placeholder} type={type} id={id} name={name} value={value} size={size} maxLength={maxLength}></input>
		);
	}
}

export default Input;
