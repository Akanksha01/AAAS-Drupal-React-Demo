import React, { Component } from 'react';

class FooterLists extends Component {

  render(){
    
    return(
      <div className="row row--footnav" lang="en">
        <div className="container">
          <footer className="site-footer" role="contentinfo">
            <ul className="nav-footer">
              <li className="expanded">
                <a href="http://www.sciencemag.org/about/about-us" title="">About us</a>
                <ul className="nav-footer__subnav">
                  <li><a href="http://www.sciencemag.org/journals" title="">Journals</a></li>
                  <li><a href="http://www.sciencemag.org/about/leadership-and-management" title="">Leadership</a></li>
                  <li><a href="http://www.sciencemag.org/about/team-members" title="">Team members</a></li>
                  <li><a href="http://www.aaas.org/page/employment-aaas" title="">Work at AAAS</a></li>
                  </ul>
              </li>
              <li className="expanded">
                <a href="https://advertising.sciencemag.org/" title="">Advertise</a>
                <ul className="nav-footer__subnav">
                  <li><a href="https://advertising.sciencemag.org/" title="">Advertising kits</a></li>
                  <li><a href="http://www.sciencemag.org/custom-publishing" title="">Custom publishing</a></li>
                </ul>
              </li>
              <li className="expanded">
                <a href="http://www.sciencemag.org/subscriptions" title="">For subscribers</a>
                <ul className="nav-footer__subnav">
                  <li><a href="http://www.sciencemag.org/librarians" title="">Site license info</a></li>
                  <li><a href="http://www.sciencemag.org/members" title="">For members</a></li>
                </ul>
              </li>
              <li className="expanded">
                <a href="http://www.sciencemag.org/about/international-versions" title="">International</a>
                <ul className="nav-footer__subnav">
                  <li><a href="http://www.sciencemagchina.cn/" title="">Chinese</a></li>
                  <li><a href="http://www.sciencemag.jp/" title="">Japanese</a></li>
                </ul>
              </li>
              <li className="expanded">
                <a href="http://www.sciencemag.org/help" title="">Help</a>
                <ul className="nav-footer__subnav">
                  <li><a href="http://www.sciencemag.org/subscriptions" title="">Access &amp; subscriptions</a></li>
                  <li><a href="http://www.sciencemag.org/help/reprints-and-permissions" title="">Reprints &amp; permissions</a></li>
                  <li><a href="http://www.sciencemag.org/about/contact-us" title="">Contact us</a></li>
                  <li><a href="http://www.sciencemag.org/about/accessibility" title="">Accessibility</a></li>
                </ul>
              </li>
              <li className="expanded">
                <a href="/about/email-alerts-and-rss-feeds" className="" title="">Stay Connected</a>
                <ul className="nav-footer__subnav">
                  <ul className="social__list">
                    <li><a href="https://www.facebook.com/ScienceMagazine" className="social__icon" title=""><i className="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/sciencemagazine" className="social__icon" title=""><i className="fa fa-twitter"></i></a></li>
                    <li><a href="https://plus.google.com/104077269810469435941/posts" className="social__icon" title=""><i className="fa fa-google-plus"></i></a></li>
                    <li><a href="/about/email-alerts-and-rss-feeds" className="social__icon" title=""><i className="fa fa-rss"></i></a></li>
                  </ul>
                </ul>
              </li>
            </ul>
          </footer>
        </div>
      </div>
    );
  }
}

export default FooterLists;

