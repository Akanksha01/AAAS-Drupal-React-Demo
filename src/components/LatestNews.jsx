import React, { Component } from 'react';
import Trending from './Trending';
import MoreRead from './MoreRead';

class LatestNews extends Component {

	constructor () {
    super();
    this.state={
    	renderComponent:<Trending/>
    }
    this.handleMore = this.handleMore.bind(this);
    this.handleTrending = this.handleTrending.bind(this);
    this.removeClassEle = this.removeClassEle.bind(this);
  }

  removeClassEle(){
  	var listele = document.getElementById("tabify-controls-popular-trending-0");
  	for (var i = listele.children.length - 1; i >= 0; i--) {
  		listele.children[i].classList.remove("tabify__controller--active");
  	};
  }

  handleMore(e) {
  	this.removeClassEle();
  	
  	e.target.parentElement.classList.add("tabify__controller--active");
  	this.setState({renderComponent: <MoreRead/>});
  }

  handleTrending(e) {
  	this.removeClassEle();
  	
  	e.target.parentElement.classList.add("tabify__controller--active");
  	this.setState({renderComponent: <Trending/>});
  }

  render(){
    return(
      <div className="layout-item">
        <h2 className="title subsection-title">Latest News</h2>
        <div className="tabify__outer-wrapper">
        	<ul className="tabify__controls" id="tabify-controls-popular-trending-0" aria-hidden="true">
        		<li className="tabify__controller--active tabify__controller" onClick={this.handleTrending}>
        			<a >Trending</a>
        		</li>
        		<li className="tabify__controller" onClick={this.handleMore}>
        			<a >Most Read</a>
        		</li>
        	</ul>
        	{ this.state.renderComponent }
          <p><a href="# " className="btn--block">More <i>Latest</i>News</a></p>
        </div>
      </div>
    );
  }
}

export default LatestNews;

