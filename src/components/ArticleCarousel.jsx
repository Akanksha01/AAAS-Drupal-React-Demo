import React, { Component } from 'react';
import moment from 'moment';

class ArticleCarousel extends Component {

  render(){
    const {article}=this.props;
    var imgurl, altimg, detailURL, title, articleDate = "";
    if (article) {
      var date = moment(new Date(article.changed[0].value));
      articleDate = date.format("MMM") + '. ' + date.format("DD") + ", "+ date.format("YYYY");
      title=article.title[0].value;
      imgurl = article.field_image[0].url;
      altimg=article.field_image[0].alt;
      detailURL = "/news/"+ date.format("YYYY") + "/" +  date.format("MM")+ "/" + article.nid[0].value+"/"+ article.title[0].value.replace(/[\. ,:-]+/g, "-").toLowerCase();
    };
   
    return(
        <div className="layout-item">
          <div className="view-carousels-and-sliders-block-2">
            <div className="aaascarousel carousel carousel--nodes slick-initialized slick-slider slick-dotted" role="toolbar">
              <div aria-live="polite" className="slick-list draggable">
                <div className="slick-track" role="listbox" >
                  <article className="hero--inset  slick-slide slick-cloned" data-slick-index="5" aria-hidden="true" tabIndex="-1">
                    <div className="hero__image">
                      <a href={detailURL} tabIndex="-1">
                      <img typeof="foaf:Image" src={imgurl} alt={altimg}/>  
                      </a>
                    </div>
                    <div className="hero__content">
                      <h2 className="hero__headline">
                        <a href={detailURL} tabIndex="-1">
                        {title}</a>
                      </h2>
                      <p className="sourceline">
                        <a href={detailURL} tabIndex="-1">
                        Latest News | {""}
                        <time>
                        <span className="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2018-08-23T11:45:00-04:00">{articleDate}</span>  
                        </time>
                        </a>
                      </p>
                      <p className="credit">NASA/JPL-CALTECH</p>
                    </div>
                  </article>
                </div>
              </div>
              <div className="carousel--nodes__nav">
                <div className="carousel--nodes__prev slick-arrow" >‹</div>
                <div className="carousel--nodes__next slick-arrow" >›</div>
              </div>
              <ul className="slick-dots" role="tablist" >
                <li className="" aria-hidden="true" role="presentation" aria-controls="navigation00" id="slick-slide00">
                  <div data-role="none" role="button" tabIndex="0"></div>
                </li>
                <li aria-hidden="false" role="presentation" aria-controls="navigation01" id="slick-slide01" className="slick-active">
                  <div data-role="none" role="button" tabIndex="0"></div>
                </li>
                <li aria-hidden="true" role="presentation" aria-controls="navigation02" id="slick-slide02" className="">
                  <div data-role="none" role="button" tabIndex="0"></div>
                </li>
                <li aria-hidden="true" role="presentation" aria-controls="navigation03" id="slick-slide03" className="">
                  <div data-role="none" role="button" tabIndex="0"></div>
                </li>
                <li aria-hidden="true" role="presentation" aria-controls="navigation04" id="slick-slide04" className="">
                  <div data-role="none" role="button" tabIndex="0"></div>
                </li>
                <li className="carousel--pause-wrapper" role="presentation" aria-controls="navigation05" id="slick-slide05" aria-hidden="true">
                  <div className="carousel--pause carousel--pause__is_playing"><span className="is-vishidden">Pause</span></div>
                </li>
              </ul>
            </div>
          </div>
        </div>
    );
  }
}

export default ArticleCarousel;

