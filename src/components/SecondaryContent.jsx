import React, { Component } from 'react';
import AdvertisementOne from './shared/AdvertisementOne';
import AdvertisementTwo from './shared/AdvertisementTwo';
import LatestNews from './LatestNews';
import Sifter from './Sifter';

class SecondaryContent extends Component {

  render(){
    
    return(
      <div className="secondary secondary" lang="en">
        <div className="layout-item cf">
          <div className="layout-item" id="paywall-hello">
            <div className="layout-item box--standout" style={{"marginBottom" : "30"}}>News from <cite>Science</cite> has <a href="//www.sciencemag.org/about/metered-access-faqs"><b>introduced a metered paywall</b></a>. Full access to all news content is included in <a href="https://www.aaas.org/hello"><b>AAAS membership.</b></a>
            </div>
          </div>
          <div className="cf" style={{"borderBottom": '1px solid #ccc'}}>
            <h2 className="subsection-title">Got a tip?</h2>
            <a className="btn btn--small align-right" style={{"marginTop" : "-2.5rem"}}>How to contact the news team</a>
          </div>
        </div>
        <AdvertisementOne/>
        <AdvertisementTwo/>
        <LatestNews/>
        <Sifter/>
        <a href="//www.sciencemag.org/tags/science-vote-2018?IntCmp=sciencevotesiderail-120">
          <img src="//www.sciencemag.org/sites/default/files/3_300x125-tout.jpg" alt="tout"/>
        </a>
      </div>
    );
  }
}

export default SecondaryContent;

  