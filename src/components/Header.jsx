import React, { Component } from 'react';
import Logo from './shared/Logo';
import Navigation from './Navigation';
import SearchBox from './SearchBox';

class Header extends Component {

	render(){
		return(
			<header className="sticky-header sticky-header-is-hidden">
				<div className="container--sticky-header">
					<Logo/>
					<Navigation/>
					<SearchBox/>						
				</div>
			</header>
		);
	}
}

export default Header;

