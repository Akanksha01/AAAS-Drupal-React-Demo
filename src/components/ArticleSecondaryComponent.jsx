import React, { Component } from 'react';
import AdvertisementOne from './shared/AdvertisementOne';
import AdvertisementTwo from './shared/AdvertisementTwo';
import LatestNews from './LatestNews';
import Sifter from './Sifter';
import ScienceInsider from './shared/ScienceInsider';

class ArticleSecondaryContent extends Component {

  render(){
    
    return(
      <div className="secondary">
        <div className="layout-item cf">
          <div className="layout-item" id="paywall-hello"></div>
          <div className="cf" style={{"borderBottom": '1px solid #ccc'}}>
            <h2 className="subsection-title">Got a tip?</h2>
            <a className="btn btn--small align-right" style={{"marginTop" : "-2.5rem"}}>How to contact the news team</a>
          </div>
        </div>
        <AdvertisementOne/>
        <ScienceInsider/>
        <Sifter/>
        <AdvertisementTwo/>
        <LatestNews/>
        <a href="//www.sciencemag.org/tags/science-vote-2018?IntCmp=sciencevotesiderail-120">
          <img src="//www.sciencemag.org/sites/default/files/3_300x125-tout.jpg" alt="tout"/>
        </a>
      </div>
    );
  }
}

export default ArticleSecondaryContent;

  