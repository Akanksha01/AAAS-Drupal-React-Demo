import React, { Component } from 'react';
import moment from 'moment';
import ArticleFigure from './ArticleFigure';
import ArticleHeader from './ArticleHeader';
import FooterContent from './FooterContent';

class ArticleContent extends Component {

  constructor(){
    super();
    this.createMarkup = this.createMarkup.bind(this);
  }

  createMarkup() {
    const {articleDetails}=this.props;
    var articleText="";
    if (articleDetails && articleDetails[0]) {
       articleText = articleDetails[0].body[0].value;
      }
    return {__html: articleText};
  }

  render(){
    const {articleDetails}=this.props;
    var imageUrl, date, articleDate, articleText, title, bannerTitle, authername="";
    if (articleDetails && articleDetails[0]) {
      imageUrl=articleDetails[0].field_image[0].url;
      date = moment(new Date(articleDetails[0].changed[0].value));
      articleDate = date.format("MMM. DD, YYYY , h:mm a");
      articleText = articleDetails[0].body[0].value;
      title = articleDetails[0].title[0].value;
      bannerTitle = articleDetails[0].field_banner_title[0].value;
      authername= articleDetails[0].field_author_name[0].value;
    };
    
    return(
      <article className="primary primary--content">
        <div className="article__body">
          <div className="entity entity-paragraphs-item paragraphs-item-image">
            <ArticleFigure imageUrl={imageUrl} bannerTitle={bannerTitle}/>
          </div>
          <ArticleHeader articleDate={articleDate} title={title} authername={authername}/>
          <div dangerouslySetInnerHTML={this.createMarkup()} />
          <span content={articleText} className="rdf-meta element-hidden"></span>
          <FooterContent/>
        </div>
      </article>
    );
  }
}

export default ArticleContent;

