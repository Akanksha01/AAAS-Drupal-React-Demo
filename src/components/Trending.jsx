import React, { Component } from 'react';

class Trending extends Component {

  render(){
    
    return(
      <div className="tabify__panels">
        <div className="force_trending tabify__panel tabify__panel--active" id="tabify-popular-trending-0-tab-0" aria-hidden="false">
          <h2 className="tabify__heading" aria-hidden="false">Trending</h2>
          <ol id="force_trending__acList" className="headline-list headline-list--decimal">
            <li>
              <h2 className="media__headline media--priority-2">
                <a>These half-billion-year-old creatures were animals—but unlike any known today</a>
              </h2>
            </li>
            <li>
              <h2 className="media__headline media--priority-2">
                <a>Could the star of <i>The Meg</i> really bite a ship in half? We took a paleobiologist to the new movie to find out</a>
              </h2>
            </li>
            <li>
              <h2 className="media__headline media--priority-2">
                <a>‘Biological passports’ help researchers track the world’s biggest fish</a>
              </h2>
            </li>
            <li>
              <h2 className="media__headline media--priority-2">
                <a>These docile foxes may hold some of the genetic keys to domestication</a>
              </h2>
            </li>
            <li>
              <h2 className="media__headline media--priority-2">
                <a>Scientists spent years on a plan to import this wasp to kill stinkbugs. Then it showed up on its own</a>
              </h2>
            </li>
          </ol>
        </div>
      </div>
    );
  }
}

export default Trending;

