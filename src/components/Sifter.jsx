import React, { Component } from 'react';

class Sifter extends Component {

  render(){
    
    return(
        <div className="layout-item">
          <h2 className="title subsection-title">Sifter</h2>
          <div className="view-article-lists-block-3">
            <ul className="headline-list  priority-2">
              <li>
                <article className="media media--var media--priority-2">
                  <div className="media__icon">
                    <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/DNA_16x9_0.jpg?itok=TFZSE_zc" alt="DNA double helix"/>  
                  </div>
                  <div className="media__body">
                    <h2 className="media__headline">
                      <a href="# ">Scientists tweak DNA in viable human embryos</a>
                    </h2>
                    <p className="byline">
                      <span className="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2018-08-20T11:30:00-04:00">Aug. 20, 2018</span>  
                    </p>
                  </div>
                </article>
              </li>
              <li>
                <article className="media media--var media--priority-2">
                  <div className="media__icon">
                    <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/sf-hearing.jpg?itok=WMCx9tuh" alt="Clusters of hair cells"/>   
                  </div>
                  <div className="media__body">
                    <h2 className="media__headline">
                      <a href="# ">Scientists pinpoint the ear protein that allows us to hear</a>
                    </h2>
                    <p className="byline">
                      <span className="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2018-08-23T14:45:00-04:00">Aug. 23, 2018</span>   
                    </p>
                  </div>
                </article>
              </li>
              <li>
                <article className="media media--var media--priority-2">
                  <div className="media__icon">
                    <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/Cretoparacucujus%20cycadophilus-1280x720.jpg?itok=twMkAgGG" alt="Beetle in amber "/>  
                  </div>
                  <div className="media__body">
                    <h2 className="media__headline">
                      <a href="# ">This beetle had a head full of pollen for 99 million years</a>
                    </h2>
                    <p className="byline">
                      <span className="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2018-08-17T11:00:00-04:00">Aug. 17, 2018</span>  
                    </p>
                  </div>
                </article>
              </li>
              <li>
                <article className="media media--var media--priority-2">
                  <div className="media__icon">
                    <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/sf-elephants_0.jpg?itok=qQKo-QK4" alt="two elephants"/>  
                  </div>
                  <div className="media__body">
                    <h2 className="media__headline">
                      <a href="# ">Elephants rarely get cancer thanks to a ‘zombie’ gene</a>
                    </h2>
                    <p className="byline">
                      <span className="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2018-08-15T12:45:00-04:00">Aug. 15, 2018</span> 
                    </p>
                  </div>
                </article>
              </li>
              <li>
                <article className="media media--var media--priority-2">
                  <div className="media__icon">
                    <img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/inline_small__1_1/public/384605main_ero_omega_centauri_full_full.jpg?itok=EYhQuL8v" alt="Omega Centauri system"/>  
                  </div>
                  <div className="media__body">
                    <h2 className="media__headline">
                      <a href="# ">No life in the Omega Centauri system, scientists say</a>
                    </h2>
                    <p className="byline">
                      <span className="date-display-single" property="dc:date" datatype="xsd:dateTime" content="2018-08-10T17:35:00-04:00">Aug. 10, 2018</span>   
                    </p>
                  </div>
                </article>
              </li>
            </ul>
            <p><a href="/news/sifter" className="btn--block">More Sifter</a></p>
          </div>
        </div>
    );
  }
}

export default Sifter;

