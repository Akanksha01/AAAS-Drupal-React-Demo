import React, { Component } from 'react';
import AuthorDetails from './AuthorDetails';
import MoreArticleListings from './MoreArticleListings';

class FooterContent extends Component {

  render(){
    
    return(
      <footer className="article__foot">
        <div className="meta-line" style={{"marginBottom" : ".5em"}}>Posted in: 
          <ul className="list-comma">
            <li>
              {" "}<a>People & Events {", "} Scientific Community</a>
            </li>
          </ul>
        </div>
        <p className="doi-line">{" "}doi:10.1126/science.aav1787</p>  
        <AuthorDetails/>
        <h2>More from News</h2> 
        <div className="view-related-content-news-article-related">
          <MoreArticleListings/>
        </div>
      </footer>
    );
  }
}

export default FooterContent;

