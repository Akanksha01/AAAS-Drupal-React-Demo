import React, { Component } from 'react';

class MoreRead extends Component {

  render(){
    
    return(
      <div className="tabify__panels">
        <div className="force_trending tabify__panel tabify__panel--active" id="tabify-popular-trending-0-tab-0" aria-hidden="false">
          <h2 className="tabify__heading" aria-hidden="false">Trending</h2>
          <ol id="force_trending__acList" className="headline-list headline-list--decimal">
            <li>
              <h2 className="media__headline media--priority-2">
                <a>Researcher at the center of an epic fraud remains an enigma to those who exposed him</a>
              </h2>
            </li>
            <li>
              <h2 className="media__headline media--priority-2">
                <a>Animal fat on ancient pottery reveals a nearly catastrophic period of human prehistory</a>
              </h2>
            </li>
            <li>
              <h2 className="media__headline media--priority-2">
                <a>Quantum chicken-or-egg experiment blurs the distinction between before and after</a>
              </h2>
            </li>
            <li>
              <h2 className="media__headline media--priority-2">
                <a>What does Trump’s pick for science adviser think about climate science? A 2014 talk offers clues</a>
              </h2>
            </li>
            <li>
              <h2 className="media__headline media--priority-2">
                <a>This bright purple ribbon—named STEVE—is an entirely new celestial phenomenon</a>
              </h2>
            </li>
          </ol>
        </div>
      </div>
      
    );
  }
}

export default MoreRead;

