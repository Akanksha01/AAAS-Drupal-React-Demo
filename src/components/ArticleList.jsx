import React, { Component } from 'react';
import ArticleListItem from './ArticleListItem';
import PropTypes from 'prop-types';

class ArticleList extends Component {

	static propTypes = {
    articleList: PropTypes.array
  };

	render(){
		var articleList = this.props.articleList.map((article, index)=>{
          return <ArticleListItem article={article} key={index}/>
        })

		return(
			<div className="view-article-lists-block-6">
        <ul className="item-list">
	        {articleList}
      	</ul>
      	<p><a href="latest-news" className="btn--block">More Latest News</a></p>
      </div>
		);
	}
}

export default ArticleList;
