import React, { Component } from 'react';

class MoreArticleListings extends Component {

  render(){
    
    return(
      <ul className="media-grid media-grid--3">
        <li>
          <article className="media media--priority-2">
            <div className="media__icon">
              <a><img src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/sonde%20launch.jpg?itok=T_XI9f1j" alt="scientific balloon launch in Greenland"/></a>  
            </div>
            <div className="media__body">
              <h2 className="media__headline">
                <a >Here are 10 topics senators could ask Trump’s science adviser nominee to address</a>
              </h2>
              <span property="dc:title" content="Here are 10 topics senators could ask Trump’s science adviser nominee to address" className="rdf-meta element-hidden"></span>
            </div>
          </article>
        </li>
        <li>
          <article className="media media--priority-2">
            <div className="media__icon">
              <a><img src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/0817_roundup_FINAL.jpg?itok=4ZbPh3Qd" alt="(left to right): Humanmade fossil; 3D concept of microorganism Emiliania Huxley; archeology site in Turkey"/></a>  
            </div>
            <div className="media__body">
              <h2 className="media__headline">
                <a>Top stories: fresh-baked fossils, cloud-seeding algae, and how ancient farmers braved global cooling</a>
              </h2>
              <span property="dc:title" content="Top stories: fresh-baked fossils, cloud-seeding algae, and how ancient farmers braved global cooling" className="rdf-meta element-hidden"></span>
            </div>
          </article>
        </li>
        <li>
          <article className="media media--priority-2">
            <div className="media__icon">
              <a><img typeof="foaf:Image" src="//www.sciencemag.org/sites/default/files/styles/grid_thumb_-_290x163__16_9_/public/720MainIllustration_HiRes.jpg?itok=M84g2U12" alt="Illustration of scientists riding a wave surrounded by sheets of paper"/></a>  
            </div>
            <div className="media__body">
              <h2 className="media__headline">
                <a>Researcher at the center of an epic fraud remains an enigma to those who exposed him</a>
              </h2>
              <span property="dc:title" content="Researcher at the center of an epic fraud remains an enigma to those who exposed him" className="rdf-meta element-hidden"></span>
            </div>
          </article>
        </li>
      </ul>

    );
  }
}

export default MoreArticleListings;

