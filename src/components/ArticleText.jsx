import React, { Component } from 'react';
import LinkItems from './shared/LinkItems';
import Time from './shared/Time';
import PropTypes from 'prop-types';

class ArticleText extends Component {

  static propTypes = {
    title: PropTypes.string,
    autherName: PropTypes.string,
    articleDate: PropTypes.string,
    detailURL: PropTypes.string
  };

	render(){
    const {title, autherName, articleDate, detailURL}=this.props;

		return(
      <div className="media__body">
        <h2 className="media__headline">
          <LinkItems redirectTo={detailURL} title={title}/>
        </h2>
        <span className="rdf-meta element-hidden"></span>
        <p className="byline">By{" "} 
          <LinkItems redirectTo={detailURL} title={autherName}/>
          <Time title={articleDate}/>
        </p>
      </div>
		);
	}
}

export default ArticleText;
