import React, { Component } from 'react';
import ArticleImage from './ArticleImage';
import ArticleText from './ArticleText';
import moment from 'moment';

class ArticleListItem extends Component {

	render(){
    const {article} = this.props;
    const imgurl = article.field_image[0].url;
    var date = moment(new Date(article.changed[0].value));
    var articleDate = date.format("MMM") + '. ' + date.format("DD") + ", "+ date.format("YYYY");
    var detailURL = "/news/"+ date.format("YYYY") + "/" +  date.format("MM")+ "/" + article.nid[0].value+"/"+ article.title[0].value.replace(/[\. ,:-]+/g, "-").toLowerCase();
    var autherName = article.field_author_name && article.field_author_name[0] ? article.field_author_name[0].value : "";
    
		return(
        <li>
          <article className="media media--var">
            <ArticleImage url={imgurl} alt={article.field_image[0].alt} detailURL={detailURL}/>
            <ArticleText title={article.title[0].value} articleDate={articleDate} detailURL={detailURL} autherName={autherName}/>
          </article>
        </li>    
		);
	}
}

export default ArticleListItem;
  