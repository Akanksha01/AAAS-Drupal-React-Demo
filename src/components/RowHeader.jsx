import React, { Component } from 'react';
const img = require('../assets/images/F1.medium.gif');

class RowHeader extends Component {

	render(){
		return(
			<header className="row--header" lang = "en">
        <div className="container container--header">
          <div className="header--leaderboard">
            <div className="leaderboard__banner">
              <div id="aaas-oas_Top">
                <img src="https://s0.2mdn.net/8580887/Non_Offer_15thAug_728x90.jpg" alt="Advertisement" border="0" width="728" height="90"/>
              </div>
            </div>
            <div className="leaderboard__sitelicense">
              <div className="user-block">
                <nav className="user-block__nav">
                  <ul>
                    <li>
                      <a data-hide-link-title="0" data-is-collapsible="0" data-is-collapsed="1" className="" data-icon-position="">Log in</a>
                    </li>
                    <li>
                      <a  data-hide-link-title="0" data-is-collapsible="0" data-is-collapsed="1" className="" data-icon-position="">My account</a>
                    </li>
                    <li>
                      <a  data-hide-link-title="0" data-is-collapsible="0" data-is-collapsed="1" className="" data-icon-position="">Contact us</a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className="sitelicense__logo"></div>
            </div>
            <div className="leaderboard__promo">
              <div className="promo-subscribe">
                <div className="promo__mag" >
                  <div className="view-header-latest-mag-block">
                    <div>
                      <img typeof="foaf:Image" src={img} width="346" height="440" alt="Current Issue Cover"/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
		);
	}
}

export default RowHeader;

