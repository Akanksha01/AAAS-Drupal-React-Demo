const ARTICLE_LIST = 'ARTICLE_LIST';
const ARTICLE_DETAILS = 'ARTICLE_DETAILS';

export function getArticleList(response) {
	
  return {
    type: ARTICLE_LIST,
    payload: response
  };
}


export function getArticleDetails(response) {
	
  return {
    type: ARTICLE_DETAILS,
    payload: response
  };
}