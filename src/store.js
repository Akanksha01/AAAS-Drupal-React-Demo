import { createStore } from 'redux';
import {newsReducer} from './reducers/newsReducer';

export const store = createStore(newsReducer);
