const initialState = {
	articleList : [],
	articleDetails : []
};

export const newsReducer = (state = initialState, action) => {
	switch (action.type) {
    case "ARTICLE_LIST":
      return Object.assign({}, state, {
        articleList: action.payload ? action.payload.data : []
      })
    case "ARTICLE_DETAILS":
    	return Object.assign({}, state, {
        articleDetails: action.payload ? action.payload.data : []
      })
    default:
      return state
  }
}
