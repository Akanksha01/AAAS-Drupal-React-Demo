import React, {Component} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Helmet from "react-helmet";
import { connect } from 'react-redux';
import ArticleCarousel from '../components/ArticleCarousel';
import ArticleList from '../components/ArticleList';
import Media from '../components/shared/Media';
import SideComponent from '../components/shared/SideComponent';
import Screenshots from '../components/shared/Screenshots';
import Topics from '../components/shared/Topics';
import ArticleSecondaryComponent from '../components/ArticleSecondaryComponent';

class Articles extends Component{

  static propTypes = {
    getArticleList: PropTypes.func,
    articleList: PropTypes.array
  };

  componentWillMount(){
    const that=this;
    
    axios.get('http://dev-atlogys.pantheonsite.io/api/v1/articleapi')
      .then(function(result){ 
        that.props.getArticleList(result);
      });
  }

  render(){
    
    return(
        <div className="row">
          <Helmet
          title="News"/>
          <div className="container">
            <div className="section-title ">
              <h1 className="section-title__headline">News</h1>
            </div>
            <div className="page primary news">
              <ArticleCarousel article={this.props.articleList[0]}/>
              <div className="two-up layout-item">
                <div className="subprime--a">
                  <div className="layout-item">
                    <h2 className="title subsection-title">Latest News</h2>
                    <ArticleList articleList={this.props.articleList}/>
                  </div>
                </div>
                <SideComponent/>
              </div>
              <h2 className="subsection-title heading--block">Featured Videos<a href="/videos">Watch more Videos</a></h2>
              <Media/>
              <Screenshots/>
              <Topics/>
            </div>
            <ArticleSecondaryComponent/>
          </div>
        </div>        
    );
  }
}

const mapStateToProps = (state) => {
  return {
    articleList : state.articleList
  }
}

const mapDispatchToProps = (dispatch) => {
  return({
    getArticleList: (response) => { dispatch({ type: "ARTICLE_LIST", payload: response }) }
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(Articles);
