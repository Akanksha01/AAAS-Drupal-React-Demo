import React, {Component} from 'react';
import axios from 'axios';
import Helmet from "react-helmet";
import { connect } from 'react-redux';
import Share from '../components/Share';
import ArticleContent from '../components/ArticleContent';
import SecondaryContent from '../components/SecondaryContent';

class ArticleDetails extends Component{

  componentWillMount(){
    const that=this;
    const id = that.props.match.params.id;
    const reqURL = 'http://dev-atlogys.pantheonsite.io/api/v1/articleapi_detail/'+id;
    
    axios.get(reqURL)
      .then(function(result){ 
        that.props.getArticleDetails(result);
      });
  }

  render(){
    const {articleDetails}=this.props;
    var helTitle="";
    if (articleDetails && articleDetails[0]) {
      helTitle = articleDetails[0].title[0].value;
    };
    
    return(
      <div className="row">
      <Helmet
          title={helTitle}/>
        <div className="container container--content">
          <Share/>
          <ArticleContent articleDetails={articleDetails} /> 
          <SecondaryContent/>
        </div>
      </div> 
    );
  }
}

const mapStateToProps = (state) => {
  return {
    articleDetails : state.articleDetails
  }
}

const mapDispatchToProps = (dispatch) => {
  return({
    getArticleDetails: (response) => { dispatch({ type: "ARTICLE_DETAILS", payload: response }) }
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleDetails);
