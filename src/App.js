import React, { Component } from 'react';
import './assets/stylesheets/style.css';
import Helmet from "react-helmet";
import { store } from './store.js';
import { Provider } from 'react-redux';
import Header from './components/Header.jsx';
import Routes from './Routes';
import RowHeader from './components/RowHeader';
import NewsLetterFooter from './components/shared/NewsLetterFooter';
import FooterLists from './components/shared/FooterLists';
import AAASFooter from './components/shared/AAASFooter';

class App extends Component {
  render() {
    return (
      <div>
        <Helmet
          htmlAttributes={{lang: "en", dir:"ltr", class:"video flexbox flexboxlegacy no-flexboxtweener flexwrap fullscreen js fonts-loaded no-videoautoplay"}}
          titleTemplate="%s | Science | AAAS"
          titleAttributes={{itemprop: "name" , lang: "en"}}
          meta={[
            {name: "viewport", content: "width=device-width, initial-scale=1.0, shrink-to-fit=no"},
            {name: "twitter:card", content: "summary_large_image"},
            {name: "twitter:site", content: "@newsfromscience"},
            {name: "twitter:site:id", content: "17089636"},
            {name: "twitter:title", content: "News"},
            {name: "twitter:image:width", content: "1280"},
            {name: "twitter:image:height", content: "720"},
            {name: "dcterms.date", content: "2014-10-30T10:27:21-04:00"},
            {name: "dc.date", content: "2014-10-30T10:27:21-04:00"},
            {name: "MobileOptimized", content: "width"},
            {name: "HandheldFriendly", content: "true"},
            {"property": "fb:admins", "content": "2703576"},
            {"property": "fb:admins", "content": "678955044"},
            {"property": "fb:admins", "content": "1400521805"},
            {"property": "fb:app_id", "content": "1478542162442556"},
            {"property": "fb:pages", "content": "100864590107"},
            {"property": "og:site_name", "content": "Science | AAAS"},
            {"property": "og:title", "content": "News"},
            {"property": "og:updated_time", "content": "2018-06-14T14:46:21-04:00"},
            {"property": "og:image:type", "content": "image/jpeg"},
            {"property": "og:image:width", "content": "1280"},
            {"property": "og:image:height", "content": "720"},
            {"property": "article:publisher", "content": "https://www.facebook.com/ScienceNOW"},
            {"property": "article:published_time", "content": "2014-10-30T10:27:21-04:00"},
            {"property": "article:modified_time", "content": "2018-06-14T14:46:21-04:00"},
            {"http-equiv": "Content-Type", "content": "text/html; charset=utf-8" },
            {"http-equiv": "cleartype", "content": "on"},
            {"http-equiv": "X-UA-Compatible", "content": "IE=edge,chrome=1"}
          ]}
        />
        <Header />
        <RowHeader/>
        <Provider store={store}>
          <Routes/>
        </Provider>
        <NewsLetterFooter/>
        <FooterLists/>
        <AAASFooter/>
      </div>
    );
  }
}

export default App;
